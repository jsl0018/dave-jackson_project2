 
import socket 
import threading
import sys
import select
import math
import random
import os

from Crypto.Util import number		#added for large prime number generation
from Crypto import Random
from Crypto.Cipher import AES
import base64
import hashlib
import time


from BitVector import *



### start GUI code ###
#import file_gui
import sys
from Tkinter import *		#required fro GUI code (frames, etc...)
#import Tkinter
import tkMessageBox
### end GUI code ###



## GLOBAL VARIABLES ##
BLOCK_SIZE=16	# AES block size

#local_bv  = BitVector( size = 20 )
local_bv = BitVector( bitstring = '00000000000000000000' )
remote_bv = BitVector( bitstring = '00000000000000000000' )

start_gui = 1
AES_KEY = -1	#uses KEY to create proper sized AES_KEY 16 or 24 bytes long
p2p_AES_KEY = -1	#uses KEY to create proper sized AES_KEY 16 or 24 bytes long
dh_state = 0
file_hash_1 = 0 	#store hash sent from server
file_hash_2 = 0 	#store hash calculated by client

sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#p2p_sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
client_id = -1
client_ip = -1
client_port = -1
local_client_id = -1

######################################################################
# function to hash a file
######################################################################
def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

######################################################################
# function to encrypt message using AES
######################################################################
def encrypt(message, passphrase):
	IV = Random.new().read(BLOCK_SIZE)
	aes = AES.new(passphrase, AES.MODE_CFB, IV)
	return base64.b64encode(IV + aes.encrypt(message))

######################################################################
# function to decrypt message using AES
######################################################################
def decrypt(encrypted, passphrase):
	encrypted = base64.b64decode(encrypted)
	IV = encrypted[:BLOCK_SIZE]
	aes = AES.new(passphrase, AES.MODE_CFB, IV)
	return aes.decrypt(encrypted[BLOCK_SIZE:])

######################################################################
# function to check if number is a prime number
######################################################################
def is_prime(n):
    if n % 2 == 0 and n > 2: 
        return False
    return all(n % i for i in range(3, int(math.sqrt(n)) + 1, 2))

######################################################################
# function to generate primitive roots of the prime number
######################################################################
def primRoots(theNum):
    #if isNotPrime(theNum):
    if not is_prime(theNum):
        raise ValueError("Sorry, the number must be prime.")
    o = 1
    roots = []
    r = 2
    while r < theNum:
        k = pow(r, o, theNum)
        while (k > 1):
            o = o + 1
            k = (k * r) % theNum
        if o == (theNum - 1):
            roots.append(r)
	    #return r
        o = 1
        r = r + 1
    return roots


### start Peer-as-server code ###
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################


class ThreadedServer(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
	#print('host and port: ' + str(host) + ' -- ' + str(port))
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
	print(" peer " + client_id + "  in server-mode now listening on port " + str(port) + "...")

    def listen(self):	
        self.sock.listen(5)
	show = 1
        while True:
            client, address = self.sock.accept()
	    if show == 1:
		show = 1
		print("listening to client: " + str(address))
            client.settimeout(60)
            threading.Thread(target = self.listenToClient,args = (client,address,client_id)).start()

    def listenToClient(self, client, address, client_id):

	BLOCK_SIZE=16	# AES block size

	##############################################################
	# variable declarations
	##############################################################
	#HOST = '10.0.0.3'# Set Host Address
	HOST = '127.0.0.1'
	PORT = 63000	# Set Port number
	bit_length = 10	# random number's bit length
	p_ = -1		# P (formerly prime_p_
	g_ = -1		# (formerly root_g_
	root_array = 0	# G
	pk1 = -1	#client-side shared key
	pk2 = -1	#server-side shared key
	b_ = -1		# server-side Private Key (formely P2)
	KEY = -1	#calculated KEY that will be used by client and server for encryption (AES)
	AES_KEY2 = -1	#uses KEY to create proper sized AES_KEY2 16 or 24 bytes long
	filehash = 0
	counter = ""	#counts number of transactions to send whole file

	##############################################################
	# generate random 'shared Prime' and 'server 'private Prime'
	##############################################################
	is_prime_check_true = False				# tracks if (P-1)/2 is also prime
	while is_prime_check_true is False:			# fetch random P until (P-1)/2 is also prime
		p_ = number.getPrime(bit_length,os.urandom)# get random P
		is_prime_check_true = is_prime((p_-1)/2)	# confirm if (P-1)/2 is also prime

	is_prime_check_true = False
	while is_prime_check_true is False:			# fetch random b_ until (b_-1)/2 is also prime
		b_ = number.getPrime(bit_length,os.urandom)	# get random b_
		is_prime_check_true = is_prime((b_-1)/2)	# check if (b_-1)/2 is also prime


	##############################################################
	# generate primitive root of 'shared Prime' to send to client
	##############################################################
	root_array = primRoots(p_)			# function for generating prim. roots of P
	random_int = random.randint(0, len(root_array)-1)	# generate random index from root array
	g_ = root_array[int(random_int)]			# use random index to select root from array
	#root_array = primRoots(19)	
	#print(str(root_array))
	#return

        size = 1024
	prior_data = ''
        while True:
            #try:

                data = client.recv(size)
		print('server got: "' + str(data) + '" from client ' + str(address))
		

#		try:
#			
#			temp_arr_2 = data.split("|")
#			if len(temp_arr) == 2:
#				data = str(temp_arr_2[0])
#				file_name = str(temp_arr_2[1])	
#				print('data = ' + str(data) + '  -- file_name = ' + str(file_name))
#			if data == "6":
#				print('data = ' + str(data) + '  -- file_name = ' + str(file_name))
#		except:
#			temp_var = "do nothing"

		
		if data == "":
		    print('client ' + str(address) + ' has disconnected.')
		    client.close()
                    return False
                if data:
		    if data == "0":
			prior_data = str(data)
			print('server dh step 0: sending p_ to client = ' + str(p_))
			#client.sendall(data)
			client.sendall(str(p_))

		    if data == "1":
			prior_data = str(data)
			print('server dh step 1: sending g_ to client = ' + str(g_))
			#client.sendall(data)
			client.sendall(str(g_))
		    if data == "2":
			prior_data = str(data)
			print('server dh step 2: no action; only returning data: ' + str(data))
			client.sendall(data)

		    if data == "3":
			prior_data = str(data)
			print('server dh step 3: no action; only returning data: ' + str(data))
			print('AES_KEY2 is ready for encryption: ' + str(AES_KEY2))
			client.sendall(data)

		    if data == "6789":	#REMOVE THIS CODE IF NO LONGER USED!!
			prior_data = str(data)
			#print('dh step 6')
			# Set the response to echo back the recieved data 
			response = data

###################################################################################################################
			print('client_id = ' + str(client_id))
			inputFilename = str(client_id) + '/sample.txt'
			outputFilename = str(client_id) + '/encrypted.txt'

									# Read in the message from the input file
			fileObj = open(inputFilename)
			content = fileObj.read()
			fileObj.close()

									# Encrypt content from file
			encryptedContent = encrypt(content,AES_KEY2)

									# Write out the encrypted content to the output file.
			outputFileObj = open(outputFilename, 'w')
			outputFileObj.write(encryptedContent)
			outputFileObj.close()

									# Generate an md5 hash of the encrypted file
			filehash = md5(outputFilename)

									# Send the file with encrypted content
			filename = outputFilename
			f = open(filename,'rb')
			l = f.read(1024)
			while (l):
			    client.send(l)
			    #print('Sent ',repr(l))
			    #counter = counter + "5"
			    l = f.read(1024)
			f.close()
			time.sleep(1)   # Delays for 1 second. You can also use a float value.
        	    	print('Done sending to address: ' + str(address))
        	    	client.sendall('complete')

			
#			if len(str(counter)) > 1:
#				counter = counter[:-1]
#			#print(str(counter))
#			doneSending = True
		    
		    else:
			abc_ = 'foobar'
			command = data
			dh_state = ''
			print('inside else: data = ' + data + ' prior_data = ' + str(prior_data))
			if data != prior_data:

				try:
					temp_arr = data.split('|')
					if len(temp_arr) == 5:
						command = temp_arr[0]
						client_id = temp_arr[1]
						client_ip = temp_arr[2]
						client_port = temp_arr[3]
						client_bv =  temp_arr[4]
					if len(temp_arr) == 3:
						command = temp_arr[0]
						client_id = temp_arr[1]
						file_id = temp_arr[2]
					if len(temp_arr) == 2:
						dh_state = temp_arr[0]
						file_name = temp_arr[1]
						#print('dh_state: ' + str(dh_state) + ' -- file_name: ' + str(file_name))
					else:
						print('no command received from client')
				except:
					print('inside catch id: 7890')
					abcd = 'do nothing'
				print

				if prior_data == "1":
					prior_data = "-1"
					pk1 = str(data)
					print('server got pk1 from client: ' + str(pk1))
					pk2 = (int(g_)**int(b_)) % int(p_)
					print('server ready to send pk2 to client :' + str(pk2))
					client.sendall(str(pk2))

					KEY = (int(pk1)**int(b_))%int(p_)		# server-K = pk1 ^ (b_) mod (p)
					KEY = str(KEY)
					AES_KEY2 = str(KEY)
			
					if len(KEY) % 2 == 0:				#Encryption Key for AES must be 16 or 24 bytes long.
						#print("make key 16 bytes long")
						while len(str(AES_KEY2)) < 16:
							AES_KEY2 += KEY
					if len(KEY) % 3 == 0:
						#print("make key 24 bytes long")
						while len(str(AES_KEY2)) < 24:
							AES_KEY2 += KEY

					#print('KEY and AES_KEY2 have been generated: ' + str(KEY) + ' -- ' + str(AES_KEY2))

				if dh_state == "6":
					#print('Encrypt and send file here...!')

					#inputFilename = str(client_id) + '/sample.txt'
					inputFilename = str(client_id) + '/' + str(file_name) + '.txt'
					outputFilename = str(client_id) + '/encrypted'

											# Read in the message from the input file
					fileObj = open(inputFilename)
					content = fileObj.read()
					fileObj.close()

											# Encrypt content from file
					encryptedContent = encrypt(content,AES_KEY2)

											# Write out the encrypted content to the output file.
					outputFileObj = open(outputFilename, 'w')
					outputFileObj.write(encryptedContent)
					outputFileObj.close()

											# Generate an md5 hash of the encrypted file
					filehash = md5(outputFilename)

											# Send the file with encrypted content
					filename = outputFilename
					f = open(filename,'rb')
					l = f.read(1024)
					while (l):
					    client.send(l)
					    #print('Sent ',repr(l))
					    #counter = counter + "5"
					    l = f.read(1024)
					f.close()
					time.sleep(1)   # Delays for 1 second. You can also use a float value.
				    	print('Done sending to address: ' + str(address))
				    	client.sendall('complete')
					os.remove(outputFilename)

				if command == 'fetch_hash':
					command = "-1"
					data = "-1"
					print('have server send hash for file just sent')
					#local_bv_1 = BitVector( bitstring = '01101001011100000000' )
					client.sendall(str(filehash))
#				if command == 'fetch':
#					print('have peer-server send encrypted file to peer-client HERE!!')
#					client.sendall(str('this is your encrypted file'))

                else:
                    raise error('Client disconnected')
            #except:
		#print('inside catch (catch id:12345)')



########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
### END Peer-as-server code ###





### START Peer connect to Server code ###
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################


######################################################################
# the client to Main-Server connection function
######################################################################   
def client_Main(ip_addr,port_num): 
    # local host IP '127.0.0.1' 
    #host = '127.0.0.1'
    host = ip_addr
  
    # Define the port on which you want to connect 
    #port = 12345
    port = port_num

    bit_length = 10	# random number's bit length
    a_ = -1		# client-side Private Key
    p_	= -1		# server shared prime number
    g_ = -1		# server shared primitive root of prime_p
    pk1 = -1		#pk1 = (root_g ^ P1) ^ prime_p
    pk2 = -1		#shared key from server
    KEY = -1		#calculated KEY that will be used by client and server for encryption (AES)
    #AES_KEY = -1	#uses KEY to create proper sized AES_KEY 16 or 24 bytes long
    #file_hash_1 = 0 	#store hash sent from server
    #file_hash_2 = 0 	#store hash calculated by client	

    is_prime_check_true = False				# tracks if (a_-1)/2 is also prime
    while is_prime_check_true is False:			# fetch random P until (a_-1)/2 is also prime
	a_ = number.getPrime(bit_length,os.urandom)	# get random P
	is_prime_check_true = is_prime((a_-1)/2)	# check if (a_-1)/2 is also prime
    #print(str(a_))
  
    #sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM) 
    try:
        sock.connect((host,int(port))) 
    except:
        print('connection already established with Main-Server (or Main-Server unavailable)')
        tkMessageBox.showinfo("Notification", "connection already established with Main-Server (or Main-Server unavailable).")

    dh_state = 0		#ready_for_p
    sock.send(str(dh_state))	#Tell server to start diffie-hellman

    while True: 
	
	if dh_state > 3:
		print('dh_state > 3, client sending: "' + str(dh_state) + '" to server')
		sock.send(str(dh_state))	#sending dh_state in other place above; can put inside if statement to only execute after state N
  
	if dh_state == 0:
		data = sock.recv(1024)
		p_ = data
		print('dh_state 0: client receives p_: ' + p_)
		data = "-1"
		dh_state = 1
		sock.send(str(dh_state))
	if dh_state == 1:
		data = sock.recv(1024)
		g_ = data
		print('dh_state 1: client receives g_: ' + g_)
		data = "-1"
		dh_state = 2
		pk1 = (int(g_)**int(a_)) % int(p_)
		print('client sending pk1 to server: ' + str(pk1))
		sock.send(str(pk1))
	if dh_state == 2:
		data = sock.recv(1024)
		print('dh_state 2: client receives pk2: ' + str(data))
		pk2 = data
		data = "-1"
		KEY = (int(pk2)**int(a_))%int(p_) # client-K = pk2 ^ (a_) mod (p)
		KEY = str(KEY)
		AES_KEY = str(KEY)

						#Encryption Key for AES must be 16 or 24 bytes long.
		if len(KEY) % 2 == 0:
			while len(str(AES_KEY)) < 16:
				AES_KEY += KEY

		if len(KEY) % 3 == 0:
			while len(str(AES_KEY)) < 24:
				AES_KEY += KEY
		#print('KEY and AES_KEY have been generated: ' + str(KEY) + ' -- ' + str(AES_KEY))

		dh_state = 3
		#print('dh_state 2: client sends dh_state: ' + str(dh_state))
		#print('AES_KEY is ready for encryption: ' + str(AES_KEY))
		sock.send(str(dh_state))

	if dh_state == 3:
		#print('dh_state 3: client data = ' + str(data))
		data = "-1"
		break
		#dh_state = 4
	if dh_state == 4:
		print('dh_state 4: client data = ' + str(data))
		data = "-1"
		#dh_state = 4
	if dh_state == 6:
		with open('received_file', 'wb') as f:  
		    #while data != "-1":
		    while True:
		        data = "-1"
		        data = sock.recv(1024)
		        if data == "-1":
		            break
			if data == "complete":
			    break
			if data == "":
			    break
		        f.write(data)

		print("closing file...")
		f.close()
		print('Successfully obtained file from server')

  	#print('dh_state = ' + str(dh_state))
#	selection = raw_input('\n\n\t-|-\n\n\tEnter 1 to continue\n\tAnything else to disconnect and return to main menu\n\n\n')

        # ask the client whether he wants to continue 
#        if str(selection) == "1": 
#	    if dh_state > 3:
#	    	dh_state += 1   #updating dh_state in correct place now; can put inside if statement to only execute after state N
#            continue
#        else: 
#            break

    # close the connection 
    #print("closing client socket connection. (reset start_gui = 1)")
    #sock.close() 
    #start_gui = 1
    #print('exiting client_Main')


########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
### END Peer connect to Server code ###



######################################################################
# the client to Main-Server connection function
######################################################################   
def p2p_Main(ip_addr,port_num,remote_file_id,remote_peer_id): 
    # local host IP '127.0.0.1' 
    #host = '127.0.0.1'
    host = ip_addr
  
    # Define the port on which you want to connect 
    #port = 12345
    port = port_num

    bit_length = 10	# random number's bit length
    a_ = -1		# client-side Private Key
    p_	= -1		# server shared prime number
    g_ = -1		# server shared primitive root of prime_p
    pk1 = -1		#pk1 = (root_g ^ P1) ^ prime_p
    pk2 = -1		#shared key from server
    KEY = -1		#calculated KEY that will be used by client and server for encryption (AES)
    #p2p_AES_KEY = -1	#uses KEY to create proper sized p2p_AES_KEY 16 or 24 bytes long
    #file_hash_1 = 0 	#store hash sent from server
    #file_hash_2 = 0 	#store hash calculated by client
    inputFilename = ''	

    is_prime_check_true = False				# tracks if (a_-1)/2 is also prime
    while is_prime_check_true is False:			# fetch random P until (a_-1)/2 is also prime
	a_ = number.getPrime(bit_length,os.urandom)	# get random P
	is_prime_check_true = is_prime((a_-1)/2)	# check if (a_-1)/2 is also prime
    #print(str(a_))
  
    p2p_sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    #try:
    p2p_sock.connect((host,int(port))) 
    #except:
        #print('connection already established with Peer-Server (or Peer-Server unavailable)')
        #tkMessageBox.showinfo("Notification", "connection already established with Peer-Server (or Peer-Server unavailable)")

    dh_state = 0		#ready_for_p
    p2p_sock.send(str(dh_state))	#Tell server to start diffie-hellman

    while True: 
	
	#if dh_state > 3:
		#print('dh_state > 3, client sending: "' + str(dh_state) + '" to server')
		#p2p_sock.send(str(dh_state))	#sending dh_state in other place above; can put inside if statement to only execute after state N
  
	if dh_state == 0:
		data = p2p_sock.recv(1024)
		p_ = data
		print('dh_state 0: client receives p_: ' + p_)
		data = "-1"
		dh_state = 1
		p2p_sock.send(str(dh_state))
	if dh_state == 1:
		data = p2p_sock.recv(1024)
		g_ = data
		print('dh_state 1: client receives g_: ' + g_)
		data = "-1"
		dh_state = 2
		#p2p_sock.send(str(dh_state))
		pk1 = (int(g_)**int(a_)) % int(p_)
		print('client sending pk1 to server: ' + str(pk1))
		p2p_sock.send(str(pk1))
	if dh_state == 2:
		data = p2p_sock.recv(1024)
		print('dh_state 2: client receives pk2: ' + str(data))
		#print('need to send pk1 to server here... pk1 = ' + str(pk1))
		pk2 = data
		data = "-1"
		KEY = (int(pk2)**int(a_))%int(p_) # client-K = pk2 ^ (a_) mod (p)
		KEY = str(KEY)
		p2p_AES_KEY = str(KEY)

						#Encryption Key for AES must be 16 or 24 bytes long.
		if len(KEY) % 2 == 0:
			while len(str(p2p_AES_KEY)) < 16:
				p2p_AES_KEY += KEY

		if len(KEY) % 3 == 0:
			while len(str(p2p_AES_KEY)) < 24:
				p2p_AES_KEY += KEY
		#print('KEY and p2p_AES_KEY have been generated: ' + str(KEY) + ' -- ' + str(p2p_AES_KEY))

		#dh_state = 3
		dh_state = 6
		command = "6|" + str(remote_file_id)
		#print('dh_state 2: client sends command: ' + str(command))
		#print('p2p_AES_KEY is ready for encryption: ' + str(p2p_AES_KEY))
		p2p_sock.send(str(command))

	if dh_state == 3:
		#print('(p2p) dh_state 3: client data = ' + str(data))
		p2p_sock.send(str('fetch_hash'))
		data = "-1"
		#break
		dh_state = 4
	if dh_state == 4:
		data = p2p_sock.recv(1024)
		#print('dh_state 4: client receives = ' + str(data))

		# Compare hash received from the server with a calculated hash of the received encrypted file
		file_hash_1 = str(data)
		file_hash_2 = md5(inputFilename)
		print('\n\nSuccessfully obtained hash from server')
		print("server_hash = " + str(file_hash_1))
		print("client_hash = " + str(file_hash_2))
		if file_hash_1 == file_hash_2:
			print("\n\n *** Success!\nFile " + str(remote_file_id) + " successfully received from Peer #" + str(remote_peer_id) + ".\n\nfile hash compare:\n" + file_hash_1 + "\n" + file_hash_2)
			tkMessageBox.showinfo("Success!", "File " + str(remote_file_id) + " successfully received from Peer #" + str(remote_peer_id) + ".\n\nfile hash compare:\n" + file_hash_1 + "\n" + file_hash_2)
		else:
			print("file hash sent from server DID NOT match md5 hash of file received")
		data = "-1"
		#dh_state = 4
		break

	if dh_state == 6:

		inputFilename = str(client_id) + '/encrypted_file.txt'
		outputFilename = str(client_id) + '/unencrypted_file.txt'

###########################################################################################################
		#with open('received_file', 'wb') as f: 
		with open(inputFilename, 'wb') as f:  
		    #while data != "-1":
		    while True:
		        data = "-1"
		        data = p2p_sock.recv(1024)
		        if data == "-1":
		            break
			if data == "complete":
			    break
			if data == "":
			    break
		        f.write(data)

		print("closing file...")
		f.close()
		print('Successfully obtained file from p2p-Server')
						# Read in the message from the input file
		fileObj = open(inputFilename)
		content = fileObj.read()
		fileObj.close()
		
						# Call decrypt()
		decryptedContent = decrypt(content,p2p_AES_KEY)

						# Write out the Decrypted content to the output file.
		outputFileObj = open(outputFilename, 'w')
		outputFileObj.write(decryptedContent)
		outputFileObj.close()

##################################################################################################################
		dh_state = 3

		

  	print('dh_state = ' + str(dh_state))
#	selection = raw_input('\n\n\t-|-\n\n\tEnter 1 to continue\n\tAnything else to disconnect and return to main menu\n\n\n')


    # close the connection 
    #print("closing client (p2p_sock) socket connection. (reset start_gui = 1)")
    p2p_sock.close() 
    #start_gui = 1
    #print('exiting p2p_Main')


########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
### END Peer connect to Peer code ###








### start GUI code ###
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################


class BTGui(Frame):
   def __init__( self, firstpeer, hops=2, maxpeers=5, serverport=5678, master=None ):
      Frame.__init__( self, master )
      self.grid()
      self.createWidgets()
      host,port = firstpeer.split(':')
      #print('host :' +  str(host) + ' -- port: ' + str(port) + ' -- serverport: ' + str(serverport))
      peer_ = host + ' - ' + port
      #self.master.title( "GUI for Peer %s" % str(firstpeer) )
      local_client_id = client_id
      self.master.title( 'GUI for Peer ID: "' + client_id + '" ( ' + client_ip + '  on port: ' + client_port + ')' )
      
      
   def createWidgets( self ):
	"""
	Set up the frame widgets
	"""
	try:
		#Main-Server File Frame
		fileFrame = Frame(self)
		fileFrame.grid(row=0, column=0, sticky=N+S)
		Label( fileFrame, text='Main-Server Files' ).grid()

		#Local File List Frame
		peerFrame = Frame(self)
		peerFrame.grid(row=0, column=1, sticky=N+S)
		Label( peerFrame, text='My File List' ).grid()

		#Rebuild FRame
		rebuildFrame = Frame(self)
		rebuildFrame.grid(row=3, column=1)

		#Search Frame
		searchFrame = Frame(self)
		searchFrame.grid(row=4)

		#Get File Frame
		addfileFrame = Frame(self)
		addfileFrame.grid(row=3)

		#bpFrame (for buttons)
		pbFrame = Frame(self)
		pbFrame.grid(row=1, column=0)

		#Main-Server File List
#######################################################################################
		fileListFrame = Frame(fileFrame)
		fileListFrame.grid(row=1, column=0)
		fileScroll = Scrollbar( fileListFrame, orient=VERTICAL )
		fileScroll.grid(row=0, column=1, sticky=N+S)

		self.fileList = Listbox(fileListFrame, height=5, 
				yscrollcommand=fileScroll.set)
		#self.fileList.insert( END, '1', '2', '3', '4', '5', '6' )	#comment back out after done testing

		self.fileList.grid(row=0, column=0, sticky=N+S)
		fileScroll["command"] = self.fileList.yview

		#Get File Info (from Main-Server)
#######################################################################################
		self.fetchButton = Button( fileFrame, text='Get File Info',
				   command=self.getFileInfo_fromMainServer)
		self.fetchButton.grid()


		#Get File from peer
#######################################################################################
		self.addfileEntry = Entry(addfileFrame, width=25)
		self.addfileButton = Button(addfileFrame, text='Get File from peer',
				   command=self.getFileFromPeer)
		self.addfileEntry.grid(row=0, column=0)
		self.addfileButton.grid(row=0, column=1)


		#Search
#######################################################################################
		self.searchEntry = Entry(searchFrame, width=25)
		self.searchButton = Button(searchFrame, text='Search', 
				   command=self.onSearch)
		self.searchEntry.grid(row=0, column=0)
		self.searchButton.grid(row=0, column=1)

		#Local Peer File List (My File List)
#######################################################################################
		peerListFrame = Frame(peerFrame)
		peerListFrame.grid(row=1, column=0)

		peerScroll = Scrollbar( peerListFrame, orient=VERTICAL )
		peerScroll.grid(row=0, column=1, sticky=N+S)

		self.peerList = Listbox(peerListFrame, height=5,
				yscrollcommand=peerScroll.set)
		#self.peerList.insert( END, '1', '2', '3', '4', '5', '6' )	#comment back out after done testing
		self.peerList.grid(row=0, column=0, sticky=N+S)
		peerScroll["command"] = self.peerList.yview

		#Remove
#######################################################################################
		self.removeButton = Button( pbFrame, text='Connect to Main-Server',
					  command=self.connectToMainServer )		
		self.removeButton.grid(row=0, column=0)

		#Refresh
#######################################################################################
		self.refreshButton = Button( pbFrame, text = 'Sync File Lists', 
				    command=self.syncMainServer )		
		self.refreshButton.grid(row=0, column=1)

		#Rebuild
#######################################################################################
		self.rebuildEntry = Entry(rebuildFrame, width=25)
		self.rebuildButton = Button( rebuildFrame, text = 'Refresh local file list', 
				    command=self.refreshLocalFileList )
		self.rebuildEntry.grid(row=0, column=0)
		self.rebuildButton.grid(row=0, column=1)      


#######################################################################################
		#print "Done"	#comment back out after done testing
		self.refreshLocalFileList()

	except:
		print('inside catch id: 2345')
      
   def getFileFromPeer(self): #Get File from Peer

      file_info = self.addfileEntry.get()
      temp_arr = str(file_info).split("|")
      remote_peer_id = temp_arr[0]
      remote_file_id = temp_arr[1]
      ip_addr = temp_arr[2]
      port_num = temp_arr[3]
      local_client_id = client_id
      requested_file = temp_arr[1]
      print('\n\np2p-Server...')
      p2p_Main(ip_addr,port_num,remote_file_id,remote_peer_id)  #ip_addr,port_num

      src = './' + str(local_client_id)+ '/' + 'unencrypted_file.txt'
      dest = './' + str(local_client_id)+ '/' + remote_file_id + '.txt'
      os.rename(src, dest)

      temp_file2delete = "./" + str(local_client_id) + "/encrypted_file.txt"
      if os.path.exists(temp_file2delete):		
	os.remove(temp_file2delete)

      #print('next command HERE!...'
      self.refreshLocalFileList()


   def onSearch(self):
	print('no Search feature enabled.')


   def onTimer( self ):
      self.syncMainServer()
      self.after( 3000, self.onTimer )

      
   def __onDestroy( self, event ):
      self.btpeer.shutdown = True


   def updatePeerList( self ):
	print('update Peer List here...')
#	if self.peerList.size() > 0:
#		self.peerList.delete(0, self.peerList.size() - 1)
#	for p in self.btpeer.getpeerids():
#		self.peerList.insert( END, p )


   def syncFileList( self ):
	try:
		if self.fileList.size() > 0:
			self.fileList.delete(0, self.fileList.size() - 1)

		#print('Sync Info with Main-Server here...')
		message = 'sync|' + str(client_id) + '|'   + str(client_ip) + '|'  + str(client_port) + '|' + str(local_bv)

		#print('send "SYNC" message to server = ' + str(message))

		sock.send(str(message))
		data = sock.recv(1024)
		remote_bv = str(data)
		#print('data from server = ' + str(remote_bv))
	
		index = 0
		if self.fileList.size() > 0:
			self.fileList.delete(0, self.fileList.size() - 1)
		for c in remote_bv:
			#print(str(c))
			if c == '0' or c == '1':
				if c == '1':
					#local_bv[int(index)]=1
					self.fileList.insert( END, str(index) )
				index += 1
	except:
		print('Must connect to Main-Server before syncing.')
		tkMessageBox.showinfo("Notification", "Must connect to Main-Server before syncing.")
			
   def getFileInfo_fromMainServer(self):
	sels = self.fileList.curselection()
	#print('fetch file: ' + str(sels))
	selected_file = ''
	if len(sels)==0:
		print('must select a file!')
		if self.fileList.size() == 0:
			tkMessageBox.showinfo("Notification", "Must connect to Main-Server and Sync first.")
		else:
			tkMessageBox.showinfo("Notification", "Please select a file from Main-Server List.")
		return
	if len(sels)==1:
		selected_file = str(self.fileList.get(sels[0]).split(':'))	
		temp_arr = selected_file.split("'")
		selected_file = temp_arr[1]
		#print(sel)

	#print('"FETCH" file info from Main-Server for file: ' + str(selected_file))
	#print('After fetch from main-server, load file info into "self.addfileEntry"')

	message = 'fetch|' + client_id + '|' + str(selected_file) # fetch cmd|my ID|selected_file

	#print('send "FETCH" message to server = ' + str(message))

	sock.send(str(message))
	data = str(sock.recv(1024))


	#key = self.addfileEntry.get()
	#self.addfileEntry.delete( 0, len(key) )
	#print('send a message to all local peer IDs. key = ' + str(key))
	self.addfileEntry.delete(0, "end") # delete all the text in the entry
	self.addfileEntry.insert(0, data)  #peer ID|file name|IP|Port#


   def connectToMainServer(self):
#      sels = self.peerList.curselection()
	#print('connect to main server here... ')
	#ip_addr = str(raw_input("IP Addr? "))
	ip_addr = "127.0.0.1"
	port_num = 12345
	#print('dh_state: ' + str(dh_state) + ' -- client-side AES Key: ' + str(AES_KEY))
	print('\n\nMain-Server...')
	client_Main(ip_addr,port_num)


   def syncMainServer(self):  #Sync File Lists
	#print('Sync file info with Main-Server here...')
	self.syncFileList()
	#print('Update local Peer List here??')
	#self.updatePeerList()
	
	


   def refreshLocalFileList(self):
#	if not self.btpeer.maxpeersreached():
	if self.peerList.size() > 0:
		self.peerList.delete(0, self.peerList.size() - 1)

	#print('rebuild local file list here...')
	#self.peerList.insert( END, '4', '5', '6', '1', '2', '3' )
	directory = "./" + client_id
	file_id = 0
	file_ext = ''
	for filename in os.listdir(directory): 
		#if filename.endswith(".asm") or filename.endswith(".py"):
		if filename.endswith(".txt"):
			#print(os.path.join(directory, filename))
			file_id,file_ext = filename.split('.')
			if file_id != 'sample':
				local_bv[int(file_id)]=1
				self.peerList.insert( END, file_id )
			continue
		else:
			continue

	# Construct a bit vector directly from a bit string:
	#local_bv = BitVector( bitstring = '00110011' )
	#print('\nConstruct a bit vector directly from a bit string while looping over local files')
	#print local_bv                                    



def gui_main():
#	if len(sys.argv) < 4:
#		print "Syntax: %s server-port max-peers peer-ip:port" % sys.argv[0]
#		sys.exit(-1)

	#python file_gui.py 12345 5 127.0.0.1:12346
	#serverport = int(sys.argv[1])
	#maxpeers = sys.argv[2]
	#peerid = sys.argv[3]
	serverport = 12345			#the main server's port
	maxpeers = 5				#not being used at the moment
	peerid = '127.0.0.1:12346'		#this peer's ID (hostIP:portID)
	app = BTGui( firstpeer=peerid, maxpeers=maxpeers, serverport=serverport )
	app.mainloop()
 


########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################
### end GUI code ###




if __name__ == '__main__': 
    try:

	############################
	#    PROCESS INPUT ARGS
	############################
	argv = sys.argv[1:]
	input_file = ''
	init = 1

	#print 'Number of arguments:', len(argv), 'arguments.'
	print 'Argument List:', str(argv[0])

	while True:

		if len(argv) == 0:
			print('must include CONFIG. FILE as first argument! \n\t(ex: "python client_1.py 1.cfg")')
			break
		else:
			file_name,file_ext = argv[0].split('.')
			input_file = file_name + '/' + argv[0]
			print('input_file: ' + str(input_file))
			try:
				readfile = open(input_file,"r")
				if init == 1:
					init = 0
			except:
				if init == 1:
					print('That config file is already assigned or does not exist. Please try again.')
				else:
					print('  Peer ' + str(client_id) + ' signing off..."Good Bye"...')
				#print('  Peer signing off..."Good Bye"...')
				break
			for line in readfile:
				client_id,client_ip,client_port = ' '.join(line.split()).split("|")

			dest = './' + str(client_id)+ '/' + 'taken_'+str(client_id) +'.cfg'
			src = input_file
			print('src: ' + src + ' -- dest: ' + dest + ' -- client_id: ' + client_id)
			#os.rename(src, dest)

		if start_gui == 1:
			selection = raw_input('\n\n\t-|-\n\n\tEnter 1 for connect mode\n\tEnter 2 for peer-as-server mode\n\n\tAnything else to disconnect and quit\n\n\n')
	
			# ask the client whether he wants to continue 
			if str(selection) == "1": 
				os.rename(src, dest)
				gui_main()
				#client_Main(ip_addr,port_num)
				continue
			if str(selection) == "2": 
				ThreadedServer('',int(client_port)).listen()
				continue
			if str(selection) == "3": 
				break
				continue
			else: 
				break			
	
			#ThreadedServer('',port_num).listen()
		else:
			abcd = 'do nothiong'

	src = './' + str(client_id)+ '/' + 'taken_'+str(client_id) +'.cfg'
	dest = input_file
	os.rename(src, dest)
    except:
	try:
		src = './' + str(client_id)+ '/' + 'taken_'+str(client_id) +'.cfg'
		dest = input_file
		os.rename(src, dest)
		print('  Peer ' + str(client_id) + ' signing off..."Good Bye"...')
	except:
		print('  Peer ' + str(client_id) + ' signing off..."Good Bye"...')
	

	 





