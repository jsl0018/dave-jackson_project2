import socket
import threading
import sys
import select
import math
import random
import os

from Crypto.Util import number		#added for large prime number generation
from Crypto import Random
from Crypto.Cipher import AES
import base64
import hashlib
import time

from BitVector import *
server_d = dict()

######################################################################
# function to hash a file
######################################################################
def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

######################################################################
# function to encrypt message using AES
######################################################################
def encrypt(message, passphrase):
	IV = Random.new().read(BLOCK_SIZE)
	aes = AES.new(passphrase, AES.MODE_CFB, IV)
	return base64.b64encode(IV + aes.encrypt(message))

	aes = AES.new(passphrase, AES.MODE_CFB, IV)
	return aes.decrypt(encrypted[BLOCK_SIZE:])

######################################################################
# function to check if number is a prime number
######################################################################
def is_prime(n):
    if n % 2 == 0 and n > 2: 
        return False
    return all(n % i for i in range(3, int(math.sqrt(n)) + 1, 2))

######################################################################
# function to generate primitive roots of the prime number
######################################################################
def primRoots(theNum):
    #if isNotPrime(theNum):
    if not is_prime(theNum):
        raise ValueError("Sorry, the number must be prime.")
    o = 1
    roots = []
    r = 2
    while r < theNum:
        k = pow(r, o, theNum)
        while (k > 1):
            o = o + 1
            k = (k * r) % theNum
        if o == (theNum - 1):
            roots.append(r)
	    #return r
        o = 1
        r = r + 1
    return roots
  
######################################################################
# class ....
######################################################################
class ThreadedServer(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
	print("server is listening on port '12345'...")

    def listen(self):	
        self.sock.listen(5)
	show = 1
        while True:
            client, address = self.sock.accept()
	    if show == 1:
		show = 1
		print("listening to client: " + str(address))
            client.settimeout(60)
            threading.Thread(target = self.listenToClient,args = (client,address)).start()

    def listenToClient(self, client, address):

	BLOCK_SIZE=16	# AES block size

	##############################################################
	# variable declarations
	##############################################################
	#HOST = '10.0.0.3'# Set Host Address
	HOST = '127.0.0.1'
	PORT = 63000	# Set Port number
	bit_length = 10	# random number's bit length
	p_ = -1		# P (formerly prime_p_
	g_ = -1		# (formerly root_g_
	root_array = 0	# G
	pk1 = -1	#client-side shared key
	pk2 = -1	#server-side shared key
	b_ = -1		# server-side Private Key (formely P2)
	KEY = -1	#calculated KEY that will be used by client and server for encryption (AES)
	AES_KEY = -1	#uses KEY to create proper sized AES_KEY 16 or 24 bytes long
	filehash = 0
	counter = ""	#counts number of transactions to send whole file

	##############################################################
	# generate random 'shared Prime' and 'server 'private Prime'
	##############################################################
	is_prime_check_true = False				# tracks if (P-1)/2 is also prime
	while is_prime_check_true is False:			# fetch random P until (P-1)/2 is also prime
		p_ = number.getPrime(bit_length,os.urandom)# get random P
		is_prime_check_true = is_prime((p_-1)/2)	# confirm if (P-1)/2 is also prime

	is_prime_check_true = False
	while is_prime_check_true is False:			# fetch random b_ until (b_-1)/2 is also prime
		b_ = number.getPrime(bit_length,os.urandom)	# get random b_
		is_prime_check_true = is_prime((b_-1)/2)	# check if (b_-1)/2 is also prime


	##############################################################
	# generate primitive root of 'shared Prime' to send to client
	##############################################################
	root_array = primRoots(p_)			# function for generating prim. roots of P
	random_int = random.randint(0, len(root_array)-1)	# generate random index from root array
	g_ = root_array[int(random_int)]			# use random index to select root from array
	#root_array = primRoots(19)	
	#print(str(root_array))
	#return

        size = 1024
	prior_data = ''
        while True:
            try:
                data = client.recv(size)
		print('server got: "' + str(data) + '" from client ' + str(address))
		if data == "":
		    print('client ' + str(address) + ' has disconnected.')
		    client.close()
                    return False
                if data:
		    if data == "0":
			prior_data = str(data)
			#print('server dh step 0: sending p_ to client = ' + str(p_))
			#client.sendall(data)
			client.sendall(str(p_))

		    if data == "1":
			prior_data = str(data)
			#print('server dh step 1: sending g_ to client = ' + str(g_))
			#client.sendall(data)
			client.sendall(str(g_))
		    if data == "2":
			prior_data = str(data)
			#print('server dh step 2: no action; only returning data: ' + str(data))
			client.sendall(data)

		    if data == "3":
			prior_data = str(data)
			#print('server dh step 3: no action; only returning data: ' + str(data))
			#print('AES_KEY is ready for encryption: ' + str(AES_KEY))
			client.sendall(data)
			data = "-1"

		    if data == "6":
			prior_data = str(data)
			#print('dh step 6')
			# Set the response to echo back the recieved data 
			response = data
			#client.send(response)

			# file to transfer
			filename='sample.txt''
			f = open(filename,'rb')
			l = f.read(1024)
			while (l):
        	        	client.sendall(l)
        	        	l = f.read(1024)
        	    	f.close()
			time.sleep(1)   # Delays for 1 second. You can also use a float value.
        	    	print('Done sending to address: ' + str(address))
        	    	client.sendall('complete')
			
		    
		    else:
			abc_ = 'foobar'
			command = ''
			#print('inside else: data = ' + data + ' prior_data = ' + str(prior_data))
			if data != prior_data:
				#"sync|127.0.0.1|12346|01101001000000000000"

				try:
					temp_arr = data.split('|')
					if len(temp_arr) == 5:
						command = temp_arr[0]
						client_id = temp_arr[1]
						client_ip = temp_arr[2]
						client_port = temp_arr[3]
						client_bv =  temp_arr[4]

					if len(temp_arr) == 3:
						command = temp_arr[0]
						client_id = temp_arr[1]
						file_id = temp_arr[2]
				except:
					print('inside catch id: 7890')
					abcd = 'do nothing'
							
		
				if prior_data == "1":
					pk1 = str(data)
					#print('server got pk1 from client: ' + str(pk1))
					pk2 = (int(g_)**int(b_)) % int(p_)
					#print('server ready to send pk2 to client :' + str(pk2))
					client.sendall(str(pk2))

					KEY = (int(pk1)**int(b_))%int(p_)		# server-K = pk1 ^ (b_) mod (p)
					KEY = str(KEY)
					AES_KEY = str(KEY)
			
					if len(KEY) % 2 == 0:				#Encryption Key for AES must be 16 or 24 bytes long.
						while len(str(AES_KEY)) < 16:
							AES_KEY += KEY
					if len(KEY) % 3 == 0:
						while len(str(AES_KEY)) < 24:
							AES_KEY += KEY

					#print('KEY and AES_KEY have been generated: ' + str(KEY) + ' -- ' + str(AES_KEY))
				if command == 'sync':
					l = list(client_bv)
					temp_bv = list(client_bv)
					for x in range(20):
					'''
					iterate through the clients bit vector and make a dictionary item for each file present
					this is done to get the file number so it can be passed into the keyStr
					'''
						for y in range(20):
							temp_bv[y] = '0'
						testBits = l[x]
						
						if testBits == '1':
							keyStr1 = (x)
							keyStr = (str(keyStr1) + '/' + str(client_id))
							valStr = (str(temp_arr[1]) + '|' + str(temp_arr[2]) + '|' + str(temp_arr[3]) + '|' + str(temp_arr[4]))
							
							server_d.update({keyStr:valStr}) #new directory entry made(ex. entry for file 2 on client_1 = '2|1' : 127.0.0.1
						else:
							x = x
						#update the servers bit vector to indicate which files are on the server by scanning the dictionary based on file number
						for key in server_d: #iterate through each key in the dictionary
							tempKey = key
							compKey = tempKey.split("/") #split the key to compare peer id's [0] indicates the file part of the key [1] indicates the clients_id
							temp_bv[int(compKey[0])] = '1' #update current server bv to match what files are on record in the dictionary 
							
					local_bv_1 = ''.join(temp_bv) #update local_bv_1 to the string bit vector that indicates all files the server has on record in dictionary

					client.sendall(str(local_bv_1))
						
				if command == 'fetch':
					low = 100 #value set to test distance between client id's bellow
					for key in server_d: #iterate through each key in the dictionary
						tempKey = key
						compKey = tempKey.split("/") #split the key to compare peer id's [0] indicates the file part of the key [1] indicates the clients_id
						'''
						a if statement that looks for all the directory keys that have the file id which was selected by the client
						'''
						if int(compKey[0]) == int(file_id): #if the key's file id matches the file id that is searched for...

							preTerm = abs(int(client_id) - int(compKey[1])) #subtract the client id of the directory entry from the current client's id
							if preTerm < low:
								low = preTerm
								closest = compKey[1] 
							# print closest #closest peer id to self

					#build the string key of the directory entry for the target client to trade with
					targKey = (str(file_id) + '/' + str(closest))

					'''
					after recieving the target key, iterate through the directory
					'''
					answer = server_d.get(targKey)
					tempA = answer.split('|')
					f_client_id = tempA[0]
					f_client_ip = tempA[1]
					f_client_port = tempA[2]
					contact = (str(f_client_id) + '|' + str(file_id) + '|' + str(f_client_ip) + '|' + str(f_client_port))
					client.sendall(contact)

                else:
			raise error('Client disconnected')
			
            except:
		print('inside catch ID: 98765')
		
		for key in server_d.copy(): #iterate through each key in the dictionary looking for the client id that is disconnecting
			tempKey = key
			compKey = tempKey.split("/")
			if(int(compKey[1]) == int(client_id)): #if this dictionary entry has this file id...
				del server_d[tempKey]		 #delete this dictionary item
                #print(server_d)
		client.close()
                return False
		xyz = 'do nothing'

if __name__ == "__main__":
	while True:
		#print("\nMain-Server's bit vector on start") 
		
		try:
			port_num = 12345
			break
		except ValueError:
			pass
	try:
		ThreadedServer('',port_num).listen()
	except:
		print('  Server says "Good Bye"...')